import {createRouter, createWebHistory} from "vue-router";

import index from "./pages/IndexPage.vue";
import test from "./pages/TestPage.vue";
import add from "./pages/AddPage.vue";
import main from "./pages/MainPage.vue";
import category from "./pages/CategoryPage.vue";
import speed from "./pages/SpeedPage.vue";
import indexLastfm from "./pages/IndexLastfmPage.vue";
import simalary from "./pages/SimalaryLastfmPage.vue";
import SpeedMainPage from "./pages/SpeedMainPage.vue";
import SpeedSettingsPage from "./pages/SpeedSettingsPage.vue";
import artist from "./pages/ArtistPage.vue";

const routes = [
    {
        path: "/",
        component: index,
        name: "index",
        meta: { requiresAuth: false },
        children: [
            {
                path: "/main",
                component: main,
                name: "main",
                meta: { requiresAuth: false },
            },
            {
                path: "/add",
                component: add,
                name: "add",
                meta: { requiresAuth: false },
            },
            {
                path: "/category/:id",
                component: category,
                name: "category.index",
                props: true,
                meta: { requiresAuth: false },
            },
        ],
    },

    {
        path: "/test",
        component: test,
        name: "test",
        // meta: { requiresAuth: false },
    },

    {
        path: "/speed",
        component: speed,
        name: "speed",
        // meta: { requiresAuth: false },
        children: [
            {
                path: "/speed/main",
                component: SpeedMainPage,
                name: "SpeedMainPage",
                meta: { requiresAuth: false },
            },
            {
                path: "/speed/settings",
                component: SpeedSettingsPage,
                name: "SpeedSettingsPage",
                meta: { requiresAuth: false },
            },
        ],
    },

    {
        path: "/lastfm",
        component: indexLastfm,
        name: "indexLastfm",
        // meta: { requiresAuth: false },
    },
    {
        path: "/simalary/:id",
        component: simalary,
        name: "simalary",
        props: true,
        // meta: { requiresAuth: false },
    },

    {
        path: "/artist",
        component: artist,
        name: "artist.index",
        props: true,
        // meta: { requiresAuth: false },
    },
];
const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;
