import './bootstrap';
import './sass/app.sass';

import { createApp, h } from 'vue';
import router from "./route";

import main from './pages/app.vue'

// import  TailwindPagination from '@/components/TailwindPagination.vue';

const app  = createApp({
    render: ()=>h(main)
});
// app.component('VueMarkdown', VueMarkdown)
app
    .use(router)
    .mount('#app')
