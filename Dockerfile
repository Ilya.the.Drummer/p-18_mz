FROM php:8.2-fpm-buster
USER root
ENV ACCEPT_EULA=Y

RUN set -uex; \
    apt-get update; \
    apt-get install -y ca-certificates curl gnupg; \
    mkdir -p /etc/apt/keyrings; \
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
      | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg; \
    NODE_MAJOR=18; \
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
    > /etc/apt/sources.list.d/nodesource.list; \
    apt-get -qy update; \
    apt-get -qy install nodejs -y \
    libpq-dev libfreetype6-dev libjpeg62-turbo-dev zlib1g-dev libzip-dev libtidy-dev libonig-dev libicu-dev locales \
    libaio1 g++ wget rsync git zip unzip libpng-dev libxrender1 \
    libfontconfig1 fontconfig libfontconfig1-dev apt-transport-https \
    && docker-php-ext-configure intl \
    && docker-php-ext-install gd pdo_mysql zip exif pcntl \
    && docker-php-ext-configure gd --with-freetype=/usr/ --with-jpeg=/usr/ \
    && docker-php-ext-install -j$(nproc) mbstring zip gd exif opcache \
    && apt-get clean && rm -rf /var/cache/apk/* && docker-php-source delete

# Set the locale
RUN locale-gen ru_RU.UTF-8
ENV LANG ru_RU.UTF-8
ENV LANGUAGE ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8
ENV TZ=Asia/Krasnoyarsk

# PHP TOOLS
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
#RUN groupadd -g 1000 www
#RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
